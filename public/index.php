<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!--CSS-->
        <link href="default.css" rel="stylesheet" type="text/css">
        <link href="bower_components/bootstrap/dist/css/bootstrap.css" rel="stylesheet">
        <!--JS-->
        <script src="bower_components/jquery/dist/jquery.js"></script>
        <script src="js/submitSht.js"></script>
        <script src="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
        <!--PHP-->
        <?php ini_set('display_errors',1); error_reporting(E_ALL); ?>
        <?php require 'shtlist.php'; ?>
    </head>

    <body>
        <div class="container-fluid">
            <div class="titlerow row">
                <?php require 'title.php'; ?>
            </div>
            <div id="posting-form" class="postrow row collapse">
                <?php require 'postingform.php'; ?>
            </div>
            <div class="row shtlistrow">
                <?php require 'shtlisthtml.php'; ?>
            </div>
        </div>
        <div class="row shttr-footer">
            <p>(C) 2016 James Hartshorn</p>
        </div>
    </body>

</html>