<?php
    function shtlist(){

        $filter = array();
        $options = array(
            "sort" => array(
                "year" => -1,
                "month" => -1,
                "day" => -1,
                "hour" => -1,
                "minute" => -1,
                "second" => -1
            ),
            "modifiers" => array(
                '$comment'   => "This is a query comment",
                '$maxTimeMS' => 100,
            ),
        );

        $query = new MongoDB\Driver\Query($filter, $options);

        $manager = new MongoDB\Driver\Manager("mongodb://localhost:27017");
        $readPreference = new MongoDB\Driver\ReadPreference(MongoDB\Driver\ReadPreference::RP_PRIMARY);
        $cursor = $manager->executeQuery("shttr.sheets", $query, $readPreference);
        $it = new \IteratorIterator($cursor);
        $it->rewind(); // Very important
        while($document = $it->current()) {

            $message = $document->message;
            $user = $document->user;
            $year = $document->year;
            $month = $document->month;
            $day = $document->day;
            $hour = $document->hour;
            $minute = $document->minute;
            $second = $document->second;

            echo "<div class=\"shtcard shadow\"><div class=\"sht-body\">".$message . "</div>  <div class=\"\"><footer><strong>" . $user."</strong> at ".$hour.":".$minute."<cite> on ".$day." ".$month." ".$year."</cite></footer></div></div><br>\n\t\t";
        
            $it->next();
        }

        

    }
?>  