$( document ).ready(function() {

	// Variable to hold request
	var request;

	// Bind to the submit event of our form
	$("#new-sht-form").submit(function(event){

	    // Abort any pending request
	    if (request) {
	        request.abort();
	    }
	    // setup some local variables
	    var $form = $(this);

	    // Let's select and cache all the fields
	    var $inputs = $form.find("input, select, button, textarea");

	    // Serialize the data in the form
	    var serializedData = $form.serialize();

	    // Let's disable the inputs for the duration of the Ajax request.
	    // Note: we disable elements AFTER the form data has been serialized.
	    // Disabled form elements will not be serialized.
	    $inputs.prop("disabled", true);

	    // Fire off the request to /form.php
	    request = $.ajax({
	        url: "/submitSht.php",
	        type: "post",
	        data: serializedData,
	        success: function() {
				//alert ("Yo! Changed again");
				$('#new-sht-form').append("<div id='message'></div>");
				$('#message').html("<p>Sht sent!</p>");	
				//$('#sht-list').replaceWith("<?php shtlist(); ?>"); <---This doesn't work
				location.reload();
				$('.toclear').val("");
    			
			}
	    });//ajax

	    // Callback handler that will be called on success
	    request.done(function (response, textStatus, jqXHR){
	        // Log a message to the console
	        console.log("Hooray, it worked!");

	    });

	    // Callback handler that will be called on failure
	    request.fail(function (jqXHR, textStatus, errorThrown){
	        // Log the error to the console
	        console.error(
	            "The following error occurred: "+
	            textStatus, errorThrown
	        );
	    });

	    // Callback handler that will be called regardless
	    // if the request failed or succeeded
	    request.always(function () {
	        // Reenable the inputs
	        $inputs.prop("disabled", false);
	    });

	    // Prevent default posting of form
	    event.preventDefault();
	});

});