// Variable to hold request
var request;

$(function() {
	$(".button").submit(function(event) {

		// Abort any pending request
	    if (request) {
	    	request.abort();
	    }


    	// validate and process form here 
    	var messagebox = $("input#messagebox").val();
    	var user = $("input#user").val();
    	var dataString = 'message='+ messagebox + '&user=' + user;
    	var $form = $(this);
    	// Serialize the data in the form
    	var serializedData = $form.serialize();		

  		//alert (dataString); return false;
  		request = $.ajax({
			type: "POST",
			url: "bin/submitSht.php",
			data: serializedData,
			success: function() {
				//alert ("Yo! Changed again");
				$('#contact_form').append("<div id='message'></div>");
				$('#message').html("<h3>Sht sent!</h3>");	
    			
			}


		
		});
		// Callback handler that will be called on success
	    request.done(function (response, textStatus, jqXHR){
	        // Log a message to the console
	        console.log("Hooray, it worked!");
	    });

	    // Callback handler that will be called on failure
	    request.fail(function (jqXHR, textStatus, errorThrown){
	        // Log the error to the console
	        console.error(
	            "The following error occurred: "+
	            textStatus, errorThrown
	        );
		    });

	    // Prevent default posting of form
    	event.preventDefault();
		//return false;
		

	});	
});
