<?php

ini_set('display_errors',1); 
 error_reporting(E_ALL);


//Set timezone
date_default_timezone_set('Europe/London');


// This path should point to Composer's autoloader
require 'vendor/autoload.php';


$client = new MongoDB\Client("mongodb://localhost:27017");
$sheets = $client->shttr->sheets;

if ($_POST['user'] == "")
{
	$username = "Anonymous";
}
else
{
	$username = $_POST['user'];
}


$sheet = $sheets->insertOne( [ 'message' => $_POST['message'] , 'user' => $username, 'year' =>  date('Y'), 'month' =>  date('M'), 'day' =>  date('d'), 'hour' =>  date('H'), 'minute' =>  date('i'), 'second' =>  date('s') ]);



?>