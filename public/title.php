<div class="col-xs-2 col-md-3">
    <img class="vertical-center pull-right img-rounded img-responsive" src="img/logo.png">
</div>
<div class="col-xs-6 col-md-6">
    <h1>Shttr</h1>
    <h2><small>Like Twitter but not as good</small></h2>
</div>
<div class="col-xs-4 col-md-3">
    <button href="#posting-form" class="vertical-center pull-right btn btn-default" type="button" value="" data-toggle="collapse"><img class="icon" src="img/compose-icon.png"></button>
</div>